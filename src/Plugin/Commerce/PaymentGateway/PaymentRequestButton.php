<?php

namespace Drupal\mmaw_stripe_payment_request\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\ErrorHelper;
use Drupal\commerce_stripe\Plugin\Commerce\PaymentGateway\Stripe;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;


/**
 * Provides Stripe Payment Request Button offsite payment.
 *
 * @CommercePaymentGateway(
 *   id = "mmaw_stripe_payment_request_button",
 *   label = @Translation("Stripe Payment Request Button"),
 *   display_label = @Translation("Stripe Payment Request Button"),
 *   payment_method_types = {"credit_card"},
 * )
 */
class PaymentRequestButton extends Stripe {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
             'stripe_account_id' => '',
             'api_logging' => '',
             'request_shipping' => FALSE,
             'request_phone' => FALSE,
             'capture_method' => 'automatic',
           ] + parent::defaultConfiguration();
  }
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    unset($form['card_capture_display']);
    $form['stripe_account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Stripe Account ID'),
      '#description' => $this->t('You can find your Stripe Account ID <a href="https://dashboard.stripe.com/settings/account" target="_blank">here</a>'),
      '#default_value' => $this->configuration['stripe_account_id'],
      '#required' => TRUE,
    ];

    $form['request_shipping'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fetching shipping data from Apple/Google Pay, disable this only if you have digital goods that dont need shipping address'),
      '#default_value' => $this->configuration['request_shipping'],
      '#required' => FALSE,
    ];

    $form['request_phone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable fetching phone number data from Apple/Google Pay'),
      '#default_value' => $this->configuration['request_phone'],
      '#required' => FALSE,
    ];

    $form['api_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log webhook messages onNotify for debugging'),
      '#default_value' => $this->configuration['api_logging'],
      '#required' => FALSE,
    ];

    $form['capture_method']= [
      '#type' => 'select',
      '#title' => t('Capturing method'),
      '#options' => array("automatic" => "Automatic", "manual" => "Manual"),
      '#default_value' =>  $this->configuration['capture_method'],
      '#required' => TRUE,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      if ($values['request_shipping'] != 0) {
        if (!\Drupal::moduleHandler()->moduleExists('commerce_shipping')) {
          $form_state->setError($form['request_shipping'], $this->t("Commerce shipping module is not enabled, you can't use \"Enable fetching shipping data from Apple/Google Pay...\""));
        }
        else {
          $storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');

          $query = $storage->getAggregateQuery();
          $query->count();
          $count = $query->execute();

          if ($count == 0){
            $link = Link::fromTextAndUrl('Shipping methods', Url::fromRoute('entity.commerce_shipping_method.collection'));
            $form_state->setError($form['request_shipping'], $this->t("You need to have at least one shipping method at %link to enable \"Enable fetching shipping data from Apple/Google Pay...\" ", ['%link' => $link->toString()]));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['stripe_account_id'] = $values['stripe_account_id'];
      $this->configuration['api_logging'] = $values['api_logging'];
      $this->configuration['request_shipping'] = $values['request_shipping'];
      $this->configuration['request_phone'] = $values['request_phone'];
      $this->configuration['capture_method'] = $values['capture_method'];
    }
  }



  public function createPayment(PaymentInterface $payment, $capture = TRUE)  {
    $order = $payment->getOrder();
    // If not specified, capture the entire amount.
    $intent_id = $order->getData('stripe_itent_id');
    $intent = PaymentIntent::retrieve($intent_id);
    $minor_unit_converter = \Drupal::service('commerce_price.minor_units_converter');
    if($intent->status !== 'succeeded'){
      throw new PaymentGatewayException('Stripe request button: Payment has not been received by stripe. order_id=' . $order->id());
    }
    if($intent->metadata->order_id !== $order->id()){
      throw new PaymentGatewayException('Stripe request button: Order IDs do not match. drupal\'s order_id=' . $order->id() . 'Stripe\' order_id=' . $intent->metadata->order_id );
    }
    if($intent->amount_received !== $minor_unit_converter->toMinorUnits($order->getTotalPrice())){
      throw new PaymentGatewayException('Stripe request button: Order price do not match payment intent order_id=' . $order->id());
    }

    $amount = $minor_unit_converter->fromMinorUnits($intent->amount_received, strtoupper($intent->currency));
    $payment->setRemoteId($order->getData('stripe_remote_id'));
    $payment->setAmount($amount);
    $payment->setState('completed');
    $payment->save();
  }
}
