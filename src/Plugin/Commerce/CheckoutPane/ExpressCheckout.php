<?php

namespace Drupal\mmaw_stripe_payment_request\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Url;

/**
 *
 * @CommerceCheckoutPane(
 *   id = "stripe_payment_request",
 *   label = @Translation("Express Checkout"),
 * )
 */
class ExpressCheckout extends CheckoutPaneBase {


  /**
   * Constructs a new PaymentInformation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Drupal\commerce_payment\PaymentOptionsBuilderInterface $payment_options_builder
   *   The payment options builder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    return (boolean)_get_express_checkout_gateway($this->order);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $payment_gateways = _get_express_checkout_gateway($this->order);
    if(!$payment_gateways)
      return NULL;

    $payment_gateway = array_shift($payment_gateways);
    $config = $payment_gateway->get('configuration');
    $minor_unit_converter = \Drupal::service('commerce_price.minor_units_converter');
    $minor_units_price = $minor_unit_converter->toMinorUnits($this->order->getTotalPrice());
    $price_currency = strtolower($this->order->getTotalPrice()->getCurrencyCode());
    $shipment = $this->order->get('shipments')->entity;

    $profile = \Drupal::entityTypeManager()->getStorage('profile')->create(['uid' => $this->order->getCustomerId(), 'type' =>'customer']);
    $shipping_address = NULL;
    if($profile && isset($profile->address->locality)){
      $shipping_address = [
        'country' => $profile->address->country_code,
        'city' => $profile->address->locality,
        'line1' => $profile->address->address_line1,
        'line2' => $profile->address->address_line2,
        'region' => $profile->address->administrative_area ,
        'postalCode' => $profile->address->postal_code,
      ];
    }

    $stripe_shipping_options = [];
    if($shipment){
      $shipping_method_storage = \Drupal::entityTypeManager()
                                        ->getStorage('commerce_shipping_method');
      $shipping_methods = $shipping_method_storage->loadMultipleForShipment($shipment);
      /* @var \Drupal\commerce_shipping\Entity\ShippingMethod $shipping_method */
      foreach ($shipping_methods as $shipping_method) {
        $shipping_method_plugin = $shipping_method->getPlugin();
        $shipping_rates = $shipping_method_plugin->calculateRates($shipment);
        $shipping_rate = reset($shipping_rates);
        $shipping_method_amount = $shipping_rate->getAmount();
        $stripe_shipping_options[] = [
          'id' => $shipping_method->id(),
          'label' => $shipping_method->getName(),
          'detail' => $shipping_rate->getService()->getLabel(),
          'amount' => $minor_unit_converter->toMinorUnits($shipping_method_amount),
        ];
      }
    }

    return [
      'wrapper' => [
        '#type' => 'container',
        '#attributes' => ['class' => 'checkout-express-checkout'],
        'wpayment_request' => [
          '#markup' => '<div id="payment-request-button"><!-- A Stripe Element will be inserted here. --></div>'
        ],
        'separator' => [
          '#markup' => '<p class="sepator"><span>' . $this->t('Or pay with card') . '</span></p>',
        ],
      ],
      '#attached' => [
        'library' => [
          'mmaw_stripe_payment_request/stripe_payment_request',
          'cosmo/commerce_checkout_pane.stripe_payment_request'
        ],
        'drupalSettings' => [
          'stripe_payment_request' => [
            'publishable_key' => $config['publishable_key'],
            'order_id' => $this->order->id(),
            'cancel_url' => $this->buildCancelUrl(),
            'return_url' => $this->buildReturnUrl(),
            'paymentRequest' => [
              'country' => 'FR',
              'currency' => $price_currency,
              'shippingOptions' => $stripe_shipping_options,
              'shippingAdress' => $shipping_address,
              'total' => [
                'label' => $this->order->getStore()->label(),
                'amount' => $minor_units_price,
              ],
              'requestPayerName' => TRUE,
              'requestPayerEmail' => TRUE,
              'requestShipping' => TRUE,
              'requestPayerPhone' => TRUE,
            ]
          ]
        ],
      ]
    ];
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl() {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl() {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE]);
  }

}

