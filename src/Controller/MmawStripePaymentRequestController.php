<?php

namespace Drupal\mmaw_stripe_payment_request\Controller;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\profile\Entity\ProfileInterface;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\ApplePayDomain;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for mmaw_stripe_payment_request routes.
 */
class MmawStripePaymentRequestController extends ControllerBase {


  /**
   * Builds the response.
   */
  public function orderGetShippingOptions(EntityInterface $commerce_order, Request $request) {

    // Transform JSON values to array
    $adresse = Json::decode($request->getContent());
    if ($commerce_order->hasItems()) {
      //if country not blocked
      // Check if we have some shipments made from before and delete them
      $shipment = _fill_order_items_in_new_shipment($commerce_order);
      $profile = \Drupal::entityTypeManager()->getStorage('profile')->create(['uid' => $commerce_order->getCustomerId(), 'type' =>'customer']);
      $profile->address->country_code = $adresse['shippingAddress']['country'];
      $profile->address->locality = $adresse['shippingAddress']['city'];
      $profile->address->postal_code = $adresse['shippingAddress']['postalCode'];
      $shipment->setShippingProfile($profile);
      $shipping_method_storage = \Drupal::entityTypeManager()
                                        ->getStorage('commerce_shipping_method');
      $shipping_methods = $shipping_method_storage->loadMultipleForShipment($shipment);
      $minor_unit_converter = \Drupal::service('commerce_price.minor_units_converter');
      // Loop through shipping methods and add them to array so it can be sent as Response to Stripe
      foreach ($shipping_methods as $shipping_method) {
        $shipment->setShippingMethod($shipping_method);
        $shipping_method_plugin = $shipping_method->getPlugin();
        $shipping_rates = $shipping_method_plugin->calculateRates($shipment);
        $shipping_rate = reset($shipping_rates);
        $shipping_method_amount = $shipping_rate->getAmount();
        $shipment->setAmount($shipping_method_amount);
        $response_shipping_methods[] = [
          'id' => $shipping_method->id(),
          'label' => $shipping_method->getName(),
          'detail' => $shipping_rate->getService()->getLabel(),
          'amount' => $minor_unit_converter->toMinorUnits($shipping_method_amount),
        ];
      }
    }
    $response['supportedShippingOptions'] = $response_shipping_methods;
    return new JsonResponse($response);
  }

  protected function createPaymentIntent(array $config, array $data, EntityInterface $order, $total_price){
    Stripe::setApiKey($config['secret_key']);
    ApplePayDomain::create(['domain_name' => \Drupal::request()->getHost()]);
    $paymentIntentData = [
      'payment_method' => $data["paymentMethodRemoteId"],
      'amount' => $total_price,
      'currency' => $data["paymentRequest"]["currency"],
      'payment_method_types' => ['card'],
      'capture_method' => $config['capture_method'],
      'description' => 'Order ' . $order->id(),
      'metadata' => ['order_id' => $order->id()],
      'confirmation_method' => 'automatic'
    ];

    return PaymentIntent::create($paymentIntentData);
  }


  public function prepareOrder(EntityInterface $commerce_order, Request $request) {
    $data = Json::decode($request->getContent());
    $gateways = _get_express_checkout_gateway($commerce_order);
    $payment_gateway = array_shift($gateways);
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $shipment = _fill_order_items_in_new_shipment($commerce_order);
    $profile = \Drupal::entityTypeManager()->getStorage('profile')->create(['uid' => $commerce_order->getCustomerId(), 'type' =>'customer']);
    $address = $data["data"]['result']['shippingAddress'];
    $profile->address->given_name = '';
    $profile->address->family_name = $address['recipient'] ?? $data["data"]["result"]["payerName"] ?? $data["data"]["result"]["paymentMethod"]["billing_details"]["name"];
    $profile->address->country_code = $address['country'];
    $profile->address->locality = $address['city'];
    $profile->address->administrative_area = $address['region'];
    $profile->address->postal_code = $address['postalCode'];
    foreach ($address['addressLine'] as $line_number => $line){
      if($line_number == 0){
        $profile->address->address_line1 = $line;
        $profile->address->address_line2 = '';
      }else{
        $profile->address->address_line2 += $line . ' ';
      }
    }
    $profile->set('field_telephone', $address['phone']);
    $profile->save();

    $shipment->setShippingProfile($profile);
    $commerce_order->setBillingProfile($profile);
    $shipping_method_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
    $selected_shipping_method = $shipping_method_storage->load($data['data']['result']['shippingOption']['id']);

    $shipment->setShippingMethod($selected_shipping_method);
    $shipping_method_plugin = $selected_shipping_method->getPlugin();
    $shipping_rates = $shipping_method_plugin->calculateRates($shipment);
    $shipping_rate = array_shift($shipping_rates);

    $shipping_service = $shipping_rate->getService();
    $shipment->setShippingService($shipping_service->getId());
    $amount = $shipping_rate->getAmount();
    $shipment->setAmount($amount);
    $shipment->save();
    $commerce_order->set('shipments', $shipment);
    $commerce_order->set('mail', $data['data']['result']['payerEmail']);
    $commerce_order->recalculateTotalPrice();
    $commerce_order->setData('stripe_remote_id', $data['data']['paymentMethodRemoteId']);
    $commerce_order->set('checkout_step', 'payment');
    $commerce_order->set('payment_gateway', $payment_gateway);
    $payment_details = [
      'stripe_payment_method_id' => $data['data']['paymentMethodRemoteId'],
      'card' => '',
      'email' => $data['data']['result']['payerEmail']
    ];
    $payment_method = $this->createPaymentMethod($profile, $payment_gateway, $data['data']['paymentMethodRemoteId']);
    $payment_gateway_plugin->createPaymentMethod($payment_method, $payment_details);
    $commerce_order->set('payment_method', $payment_method);
    //$payment = $this->createPayment($payment_gateway, $commerce_order);
    //$payment_gateway_plugin->createPayment($payment);

    $payment_intent = $payment_gateway_plugin->createPaymentIntent($commerce_order);

    $commerce_order->setData('stripe_itent_secret', $payment_intent->client_secret);
    $commerce_order->setData('stripe_itent_id', $payment_intent->id);
    $commerce_order->lock();
    $commerce_order->save();

    $minor_unit_converter = \Drupal::service('commerce_price.minor_units_converter');
    $total_price = $minor_unit_converter->toMinorUnits($commerce_order->getTotalPrice());
    $response = [
      'intent_secret' => $payment_intent->client_secret,
      'payment_method_remote_id' => $data['data']["paymentMethodRemoteId"],
      'amount' => $total_price,
    ];
    return new JsonResponse($response);
  }


  /**
   * Creates the payment to be processed.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway in use.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface
   *   The created payment.
   */
  protected function createPaymentMethod(ProfileInterface $profile, PaymentGatewayInterface $payment_gateway, string $remote_id) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = PaymentMethod::create([
                                              'type' => 'credit_card',
                                              'payment_gateway' => $payment_gateway->id(),
                                              'payment_gateway_mode' => $payment_gateway->get('mode'),
                                              'billing_profile' => $profile,
                                              'reusable' => FALSE,
                                              'remote_id' => $remote_id
                                            ]);
    $payment_method->save();
    return $payment_method;
  }


}
