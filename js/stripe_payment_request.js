(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.stripePaymentRequestBtn = {
    attach: function (context) {
      var stripe = Stripe(drupalSettings.stripe_payment_request.publishable_key, {
        apiVersion: "2020-08-27",
      });
      var paymentRequest = stripe.paymentRequest(drupalSettings.stripe_payment_request.paymentRequest);
      var elements = stripe.elements();
      var $checkoutPanes = $('.checkout-panes');
      var prButton = elements.create('paymentRequestButton', {
        paymentRequest: paymentRequest,
        style: {
          paymentRequestButton: {
            height: '56px',
            theme: 'dark',
          },
        },
      });

      // Check the availability of the Payment Request API first.
      paymentRequest.canMakePayment().then(function(result) {
        if (result) {
          prButton.mount('#payment-request-button');
        } else {
          document.getElementById('payment-request-button').style.display = 'none';
          document.getElementsByClassName('checkout-express-checkout')[0].style.display = 'none';
        }
      });

      paymentRequest.on('shippingaddresschange', function(ev) {
        // Perform server-side request to fetch shipping options
        fetch('/checkout/stripe_payment_request_button/shipping_options/' + drupalSettings.stripe_payment_request.order_id + '', {
          method: "POST",
          body: JSON.stringify({
            shippingAddress: ev.shippingAddress,
          }),
        }).then(function(response) {
          return response.json();
        }).then(function(result) {
          if (result.supportedShippingOptions && result.supportedShippingOptions.length) {
            ev.updateWith({
              status: 'success',
              shippingOptions: result.supportedShippingOptions,
              total: {
                amount: drupalSettings.stripe_payment_request.paymentRequest.total.amount + result.supportedShippingOptions[0].amount,
                label: drupalSettings.stripe_payment_request.paymentRequest.total.label
              }
            });
          } else {
            ev.updateWith({
              status: 'invalid_shipping_address'
            });
          }
        });
      });

      paymentRequest.on('shippingoptionchange', function(event) {
        var updateWith = event.updateWith;
        var shippingOption = event.shippingOption;
        // handle shippingoptionchange event
        var updateDetails = {
          status: 'success',
          total: {
            amount: drupalSettings.stripe_payment_request.paymentRequest.total.amount + shippingOption.amount,
            label: drupalSettings.stripe_payment_request.paymentRequest.total.label
          }
        };
        // call event.updateWith within 30 seconds
        updateWith(updateDetails);
      });
      paymentRequest.on('cancel', function() {
        // handle cancel event
        $checkoutPanes.removeClass('loading');
      });

      paymentRequest.on('paymentmethod', function(ev) {
        var paymentIntentData = {
          paymentMethodRemoteId: ev.paymentMethod.id,
          result: ev,
          paymentRequest: drupalSettings.stripe_payment_request.paymentRequest,
        };

        fetch('/checkout/stripe_payment_request_button/prepare_order/' + drupalSettings.stripe_payment_request.order_id + '', {
          method: 'POST',
          body: JSON.stringify({
            data: paymentIntentData
          }),
          headers: {
            'content-type': 'application/json'
          },
        }).then(function(response) {
          return response.json();
        }).then(function(result) {
          stripe.confirmCardPayment(
            result.intent_secret,
            {payment_method: result.payment_method_remote_id},
            {handleActions: false}
          ).then(function(confirmResult) {
            if (confirmResult.error) {
              // Report to the browser that the payment failed, prompting it to
              // re-show the payment interface, or show an error message and close
              // the payment interface.
              ev.complete('fail');
              Drupal.commerceStripe.displayError(confirmResult.error.message);
            } else {
              // Report to the browser that the confirmation was successful, prompting
              // it to close the browser payment method collection interface.
              ev.complete('success');
              // Check if the PaymentIntent requires any actions and if so let Stripe.js
              // handle the flow. If using an API version older than "2019-02-11"
              // instead check for: `paymentIntent.status === "requires_source_action"`.
              if (confirmResult.paymentIntent.status === "requires_action") {
                // Let Stripe.js handle the rest of the payment flow.
                $checkoutPanes.addClass('loading');

                stripe.confirmCardPayment(result.intent_secret).then(function(result) {
                  if (result.error) {
                    // The payment failed -- ask your customer for a new payment method.
                    window.location.href = drupalSettings.stripe_payment_request.cancel_url;
                  } else {
                    // The payment has succeeded.
                    ev.complete('success');
                    window.location.href = drupalSettings.stripe_payment_request.return_url;

                  }
                });
              } else {
                // The payment has succeeded.
                ev.complete('success');
                window.location.href = drupalSettings.stripe_payment_request.return_url;
                $checkoutPanes.addClass('loading');
              }
            }
          });
        });
        // Confirm the PaymentIntent without handling potential next actions (yet).
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
