Instructions:
 - Install the module
 - Create a "Stripe Payment Request Button" payment method
 - add the "Express Checkout" checkout pane in the order information step of your checkout flow
 - flush cash and see if it works ;)
